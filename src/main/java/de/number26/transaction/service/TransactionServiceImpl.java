package de.number26.transaction.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import de.number26.transaction.dummydata.DummyTransaction;
import de.number26.transaction.exception.TransactionException;
import de.number26.transaction.model.Transaction;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	/**
	 * create transaction
	 */
	@Override
	public void createTransaction(final Long transactionId, final Transaction transaction) throws TransactionException {
		if (transactionId == null || transactionId < 1) {
			throwException("Invalid Transaction Id, can't be created successfull transaction!");
		}
		if (transaction.getParentId() != null && transaction.getParentId() > 0) {
			final Transaction parentTransaction = DummyTransaction.getData().get(transaction.getParentId());
			if (parentTransaction == null) {
				throwException("Wrong parent transaction Id!");
			}
		}
		if (transaction.getAmount() == null || !StringUtils.hasText(transaction.getType())) {
			throwException("Incorrect data for a complete transaction!");
		}
		DummyTransaction.getData().put(transactionId, transaction);
	}

	/**
	 * get a transaction by given id
	 */
	@Override
	public Transaction getTransactionById(final Long transactionId) throws TransactionException {
		final Transaction transactionExist = DummyTransaction.getData().get(transactionId);
		if (transactionExist == null) {
			throwException("Transaction not avilable. Wrong transaction ID!");
		}
		return transactionExist;
	}

	/**
	 * return list of transaction ids those type are same
	 * 
	 */
	@Override
	public List<Long> getTransactionIdsByType(final String type) throws TransactionException {
		if (!StringUtils.hasText(type)) {
			throwException("Transaction Type can't be null or empty!");
		}
		final List<Long> transactionIds = new ArrayList<Long>();
		for (final Entry<Long, Transaction> entry : DummyTransaction.getData().entrySet()) {
			if (type.equalsIgnoreCase(entry.getValue().getType())) {
				transactionIds.add(entry.getKey());
			}
		}
		return transactionIds;
	}

	/**
	 * return sum of amount, given by a transaction id and associated with parent id.
	 * 
	 */
	@Override
	public Double getAmountSumByTransactionId(final Long transactionId) throws TransactionException {
		final Transaction transactionExist = DummyTransaction.getData().get(transactionId);
		if (transactionId == null || transactionId < 1 || transactionExist == null) {
			throwException("Invalid Transaction Id!");
		}
		Double amount = transactionExist.getAmount();
		for (final Entry<Long, Transaction> entry : DummyTransaction.getData().entrySet()) {
			if (!transactionId.equals(entry.getKey()) && transactionId.equals(entry.getValue().getParentId())) {
				amount = amount + entry.getValue().getAmount();
			}
		}
		return amount;
	}

	private void throwException(final String message) throws TransactionException {
		throw new TransactionException(message);
	}

}
