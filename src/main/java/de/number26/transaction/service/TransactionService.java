package de.number26.transaction.service;

import java.util.List;

import org.springframework.stereotype.Service;

import de.number26.transaction.exception.TransactionException;
import de.number26.transaction.model.Transaction;

@Service
public interface TransactionService {

	/**
	 * create transaction with given transaction object
	 * 
	 * @param transactionId
	 * @param transaction
	 */
	void createTransaction(Long transactionId, Transaction transaction) throws TransactionException;

	/**
	 * return transaction object by given transactionId
	 * 
	 * @param transactionId
	 * @return
	 * @throws TransactionException
	 */
	Transaction getTransactionById(Long transactionId) throws TransactionException;

	/**
	 * return list of transaction ids by given type
	 * 
	 * @return
	 * @throws TransactionException
	 */
	List<Long> getTransactionIdsByType(String type) throws TransactionException;
	
	/**
	 * return sum value of total amount by given a transaction id and associated parent id.
	 *  
	 * @return
	 * @throws TransactionException
	 */
	Double getAmountSumByTransactionId(Long transactionId)  throws TransactionException;
}
