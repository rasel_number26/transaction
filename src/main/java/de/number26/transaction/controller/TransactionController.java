package de.number26.transaction.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.number26.transaction.exception.TransactionException;
import de.number26.transaction.model.Amount;
import de.number26.transaction.model.Status;
import de.number26.transaction.model.Transaction;
import de.number26.transaction.service.TransactionService;
import de.number26.transaction.util.TransactionRestURIConstants;

/**
 * Handles requests for the Transaction service.
 */
@RestController
public class TransactionController {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionController.class);

	@Autowired
	private TransactionService transactionService;

	/**
	 * api for create transaction
	 * 
	 * @param transactionId
	 * @param transaction
	 * @return
	 */
	@RequestMapping(value = TransactionRestURIConstants.CREATE_TRANSACTION, method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Status> createTransaction(@PathVariable("transactionId") final Long transactionId,
			@RequestBody final Transaction transaction) {
		try {
			transactionService.createTransaction(transactionId, transaction);
		} catch (Exception e) {
			LOG.error("Transaction creation failed :", e);
			return new ResponseEntity<Status>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Status>(new Status("ok"), HttpStatus.OK);
	}

	/**
	 * api for transcation by transactionid
	 * 
	 * @param transactionId
	 * @return
	 */
	@RequestMapping(value = TransactionRestURIConstants.GET_TRANSACTION, method = RequestMethod.GET)
	public ResponseEntity<Transaction> getTransactionById(@PathVariable("transactionId") final Long transactionId) {
		Transaction transaction = null;
		try {
			transaction = transactionService.getTransactionById(transactionId);
		} catch (TransactionException e) {
			LOG.error("Get transaction by transactionId failed :", e);
			return new ResponseEntity<Transaction>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
	}

	/**
	 * return list of transaction id those type is same
	 * 
	 * @param type
	 * @return
	 */
	@RequestMapping(value = TransactionRestURIConstants.GET_TYPE, method = RequestMethod.GET)
	public ResponseEntity<List<Long>> getTransactionIdsByType(@PathVariable("type") final String type) {
		List<Long> transactionIds = null;
		try {
			transactionIds = transactionService.getTransactionIdsByType(type);
		} catch (TransactionException e) {
			LOG.error("Get transaction by transactionId failed :", e);
			return new ResponseEntity<List<Long>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<Long>>(transactionIds, HttpStatus.OK);
	}

	/**
	 * return sum of amount of a transaction and associated its parents id transactions
	 * 
	 * @param transactionId
	 * @return
	 */
	@RequestMapping(value = TransactionRestURIConstants.GET_SUM, method = RequestMethod.GET)
	public ResponseEntity<Amount> getAmountSumByTransactionId(@PathVariable("transactionId") final Long transactionId) {
		Double amount = null;
		try {
			amount = transactionService.getAmountSumByTransactionId(transactionId);
		} catch (TransactionException e) {
			LOG.error("Get transaction by transactionId failed :", e);
			return new ResponseEntity<Amount>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Amount>(new Amount(amount), HttpStatus.OK);
	}
}
