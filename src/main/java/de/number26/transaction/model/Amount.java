package de.number26.transaction.model;

public class Amount {

	private Double sum;

	public Amount() {
	}

	public Amount(Double sum) {
		this.sum = sum;
	}

	/**
	 * @return the sum
	 */
	public Double getSum() {
		return sum;
	}

	/**
	 * @param sum
	 *            the sum to set
	 */
	public void setSum(Double sum) {
		this.sum = sum;
	}

}
