package de.number26.transaction.dummydata;

import java.util.HashMap;
import java.util.Map;

import de.number26.transaction.model.Transaction;

/**
 * dummy data storage
 * 
 * 
 */
public class DummyTransaction {

	private static Map<Long, Transaction> data;

	/**
	 * @return the data
	 */
	public static Map<Long, Transaction> getData() {
		if (data == null) {
			data = new HashMap<Long, Transaction>();
		}
		return data;
	}

}
