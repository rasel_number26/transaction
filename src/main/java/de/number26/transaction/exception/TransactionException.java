package de.number26.transaction.exception;

public class TransactionException extends Exception {

	private static final long serialVersionUID = -2083294634264921162L;

	/**
	 * Instantiates a new Transaction Exception.
	 */
	public TransactionException() {
		super();
	}

	/**
	 * Instantiates a new Transaction Exception.
	 * 
	 * @param message
	 *            is the message.
	 */
	public TransactionException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new Transaction Exception.
	 * 
	 * @param message
	 *            is the message.
	 * @param cause
	 *            is the cause.
	 */
	public TransactionException(String message, Throwable cause) {
		super(message, cause);
	}

}
