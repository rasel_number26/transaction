package de.number26.transaction.util;

public class TransactionRestURIConstants {

	public static final String CREATE_TRANSACTION = "/transaction/{transactionId}";

	public static final String GET_TRANSACTION = "/transaction/{transactionId}";

	public static final String GET_TYPE = "/types/{type}";

	public static final String GET_SUM = "/sum/{transactionId}";

}
