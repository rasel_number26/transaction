package de.number26.transaction;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import de.number26.transaction.exception.TransactionException;
import de.number26.transaction.model.Amount;
import de.number26.transaction.model.Status;
import de.number26.transaction.model.Transaction;

@RunWith(JUnit4.class)
public class TestTransactionService {

	public static final String SERVER_URI = "http://localhost:8080/transactionservice/";

	private RestTemplate restTemplate;

	@Before
	public void setup() {
		restTemplate = new RestTemplate();
	}

	@Test
	public void testCreateTransaction() throws TransactionException {
		final Transaction transaction = new Transaction();
		transaction.setAmount(1000.00);
		transaction.setType("Cars");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Transaction> entity = new HttpEntity<Transaction>(transaction, headers);
		ResponseEntity<Status> response = restTemplate.exchange(SERVER_URI + "transaction/1", HttpMethod.PUT, entity,
				Status.class);
		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assert.assertEquals("ok", response.getBody().getStatus());
		transaction.setAmount(5000.00);
		transaction.setType("Cars");
		transaction.setParentId(1L);
		entity = new HttpEntity<Transaction>(transaction, headers);
		response = restTemplate.exchange(SERVER_URI + "transaction/2", HttpMethod.PUT, entity, Status.class);
		Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assert.assertEquals("ok", response.getBody().getStatus());
	}

	@Test
	public void testGetTransactionById() throws TransactionException {
		Transaction transaction = restTemplate.getForObject(SERVER_URI + "/transaction/1", Transaction.class);
		Assert.assertNotNull(transaction);
	}

	@Test
	public void testGetTransactionIdsByType() throws TransactionException {
		List<Long> transactionIds = restTemplate.getForObject(SERVER_URI + "/types/Cars", List.class);
		Assert.assertNotNull(transactionIds);
		Assert.assertNotSame(transactionIds.size(), 0);
	}

	@Test
	public void testGetAmountSumByTransactionId() throws TransactionException {
		Amount amount = restTemplate.getForObject(SERVER_URI + "/sum/1", Amount.class);
		Assert.assertEquals(amount.getSum(),(Double)6000.00);
		
		amount = restTemplate.getForObject(SERVER_URI + "/sum/2", Amount.class);
		Assert.assertEquals(amount.getSum(),(Double)5000.00);
	}

}
